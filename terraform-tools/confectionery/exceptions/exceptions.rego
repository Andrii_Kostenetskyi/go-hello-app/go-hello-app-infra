package fugue.regula.config

waivers[waiver] {
    waiver := {
        "rule_name": "eks_public_endpoint",
    } 
} {    
    waiver := {
        "rule_name": "vpc_igw_creation_block",
    }
} {
    waiver := {
        "rule_name": "vpc_flow_log",
    }
} {
    waiver := {
        "rule_name": "eks_controlplane_logging",
    }
}
