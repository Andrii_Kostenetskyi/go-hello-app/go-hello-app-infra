variable "name" {
  default = "docs-devops-cluster"
}

variable "project" {
  default = "docs-devops"
}

variable "location" {
  default = "europe-central2"
}

variable "initial_node_count" {
  default = 1
}

variable "machine_type" {
  default = "n1-standard-1"
}


